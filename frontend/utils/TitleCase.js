export const toTitleCase = str =>
  str
    .charAt(0)
    .toUpperCase()
    .concat(str.slice(1))

export const toTitleCaseSentence = (str, separator = " ") =>
  str
    .split(separator)
    .map(word => toTitleCase(word))
    .join(separator)

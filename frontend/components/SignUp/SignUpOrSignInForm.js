import { Form, Button } from "semantic-ui-react"
import { toTitleCase, toTitleCaseSentence } from "../../utils/TitleCase"

export const SignUpOrSignInForm = ({ onSubmit, context }) => {
  return (
    <Form onSubmit={onSubmit}>
      {context === "sign_up" && <FormInput icon="user" name="name" />}
      <FormInput icon="mail" name="email" />
      <FormInput icon="lock" name="password" />
      <Button
        content={toTitleCaseSentence(context.replace(/_/g, " "))}
        size="big"
        primary
        icon={context === "sign_up" ? "signup" : "sign-in"}
      />
    </Form>
  )
}

const FormInput = ({ icon, name }) => (
  <Form.Input
    icon={icon}
    iconPosition="left"
    label={toTitleCase(name)}
    placeholder={name}
    type={name}
    required
    name={name}
    fluid
    minLength="5"
    maxLength="30"
  />
)

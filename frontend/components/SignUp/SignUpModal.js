import { toTitleCaseSentence } from '../../utils/TitleCase'
import { Modal, Segment, Grid, Button, Divider } from 'semantic-ui-react'
import { SignUpOrSignInForm } from './SignUpOrSignInForm'

export const SignUpModal = props => {
  const handleSubmit = evt => {
    if (props.context === 'login') {
    } else {
      console.log(evt.target.name.value)
    }
  }

  return (
    <Modal
      closeIcon="close"
      size="tiny"
      trigger={props.trigger}
      className="info-modal"
      open={props.modalOpen}
      onClose={props.handleClose}
    >
      <Modal.Header className="modal-header">
        TShirt Shop | {toTitleCaseSentence(props.context.replace(/_/g, ' '))}
      </Modal.Header>
      <Modal.Content className="modal-content">
        <Segment placeholder>
          <Grid columns={2} relaxed="very" stackable>
            <Grid.Column>
              <SignUpOrSignInForm
                onSubmit={handleSubmit}
                context={props.context}
              />
            </Grid.Column>

            <Grid.Column verticalAlign="middle">
              <Button content="Sign up" icon="signup" size="big" positive />
            </Grid.Column>
          </Grid>

          <Divider vertical>Or</Divider>
        </Segment>
      </Modal.Content>
      <Modal.Actions className="modal-footer">
        <span className="modal-footer-span" />
      </Modal.Actions>
    </Modal>
  )
}

import { Segment, Icon } from 'semantic-ui-react'
import './styles.css'

export const CartItem = ({ cartItem }) => (
  <Segment className="cart-item">
    <Icon name="window close outline" color="red" size="big" />
    <p>
      {cartItem.quantity} <Icon name="cancel" color="grey" size="small" />{' '}
      {cartItem.name}
    </p>
    <span>Size: {cartItem.size}</span>
    <br />
    <span>Color: {cartItem.color}</span>
  </Segment>
)

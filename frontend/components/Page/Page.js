import { Header } from "../Layout/Header"
import { Main } from "../Layout/Main"
import "./styles.css"
import Meta from "../Layout/Header/Meta"
import { ProductsProvider } from "../../context/ProductsContext"
import { useProducts } from "../../hooks/useProducts"
import { DepartmentProvider } from "../../context/DepartmentContext"
import { useDepartments } from "../../hooks/useDepartments"

export const Page = ({ children }) => {
  const [state, setState] = useProducts()
  const [departments] = useDepartments()

  return (
    <DepartmentProvider value={[departments]}>
      <ProductsProvider value={[state, setState]}>
        <div className="app">
          <Meta />
          <Header />
          <Main>{children}</Main>
        </div>
      </ProductsProvider>
    </DepartmentProvider>
  )
}

import { Button, Card } from "semantic-ui-react"

export const ColorOptions = ({ colors, onColorClick, selectedColor }) => {
  const productColors = colors.map(color => color.toLowerCase())

  return (
    <Card.Content extra>
      <span>Color: {selectedColor}</span>
      <br />
      {productColors.map(productColor => {
        const selected = productColor === selectedColor ? "selected" : ""
        return (
          <Button
            style={{ background: productColor }}
            className={`product__color ${selected}`}
            circular
            name={productColor}
            key={productColor}
            onClick={evt => onColorClick(evt.target.name)}
          />
        )
      })}
    </Card.Content>
  )
}

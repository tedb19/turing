import { useState } from "react"
import Link from "next/link"
import "./styles.css"
import { Card, Image, Button } from "semantic-ui-react"

import { ColorOptions } from "./ColorOptions"
import { SizeOptions } from "./SIzeOptions"
import { ProductQuantity } from "./ProductQuantity"
import { Price } from "./Price"
import { ProductModal } from "./ProductModal"

export const ProductCard = ({
  product,
  ProductAttributes,
  isDetail,
  onColorClick,
  onSizeClick,
  selectedColor,
  selectedSize,
  onQuantityChange,
  quantity
}) => {
  const { Size, Color } = ProductAttributes
  const [dimmerActive, setDimmerActive] = useState(false)

  const handleToggle = () => {
    setDimmerActive(!dimmerActive)
  }

  const productDescription = isDetail
    ? product.description
    : product.description.length > 80
    ? `${product.description.slice(0, 80)}...`
    : product.description

  return (
    <Card raised color="teal" className="product-card" centered fluid>
      <Image
        src={`/static/product_images/${product.image}`}
        wrapped
        ui={false}
      />
      <Card.Content>
        <Card.Header>{product.name}</Card.Header>
        <Card.Meta>{productDescription}</Card.Meta>
      </Card.Content>

      {(dimmerActive || isDetail) && (
        <>
          <ColorOptions
            colors={Color || []}
            onColorClick={onColorClick}
            selectedColor={selectedColor}
          />
          <SizeOptions
            onSizeClick={onSizeClick}
            sizes={Size || []}
            selectedSize={selectedSize}
          />
          <ProductQuantity
            onQuantityChange={onQuantityChange}
            quantity={quantity}
          />
        </>
      )}

      <Price
        price={product.price}
        discountedPrice={product.discountedPrice}
        quantity={quantity}
      />
      <Card.Content extra className="add_to_cart">
        <ProductLink id={product.productId} />
        <ProductModal
          trigger={
            <Button
              icon="add to cart"
              label="Add To Cart"
              positive
              size="medium"
              color="teal"
            />
          }
          product={product}
        />
      </Card.Content>
    </Card>
  )
}

const ProductLink = ({ id }) => (
  <Link href={`/products/${id}`}>
    <Button
      icon="clipboard list"
      label="More Details"
      className="more-details"
      size="medium"
    />
  </Link>
)

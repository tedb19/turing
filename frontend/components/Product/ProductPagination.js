import { useContext } from "react"
import { ProductsContext } from "../../context/ProductsContext"
import { Pagination } from "semantic-ui-react"

export const ProductPagination = () => {
  const [state, setState] = useContext(ProductsContext)

  const handlePaginationChange = (e, { activePage }) => {
    setState(state => ({ ...state, page: activePage }))
  }

  return (
    <Pagination
      activePage={state.page}
      onPageChange={handlePaginationChange}
      totalPages={state.pages}
      boundaryRange={2}
      siblingRange={2}
      inverted
      size="massive"
      className="products-pagination"
    />
  )
}

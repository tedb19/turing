import { Dropdown, Card } from "semantic-ui-react"

export const SizeOptions = ({ sizes, onSizeClick, selectedSize }) => {
  const options = sizes.map(size => ({
    key: size,
    text: size,
    value: size
  }))
  return (
    <Card.Content extra>
      <span>Size: {selectedSize}</span>
      <br />
      <Dropdown
        placeholder="Select Size"
        fluid
        selection
        value={selectedSize}
        options={options}
        onChange={e => onSizeClick(e.target.textContent)}
      />
    </Card.Content>
  )
}

import { useContext } from "react"
import { ProductsContext } from "../../context/ProductsContext"
import { ProductCard } from "./ProductCard"
import { Loader } from "semantic-ui-react"

export const Products = () => {
  const [state] = useContext(ProductsContext)
  return (
    <div className="products">
      {state.loading && <Loader active size="small" />}
      {state.products.map(product => (
        <ProductCard
          product={product}
          key={product.name}
          isDetail={false}
          ProductAttributes={{}}
          quantity={1}
        />
      ))}
    </div>
  )
}

import { useContext } from "react"
import { Breadcrumb } from "semantic-ui-react"
import { ProductsContext } from "../../context/ProductsContext"
import { DepartmentContext } from "../../context/DepartmentContext"

export const BreadCrumbs = () => {
  const [state] = useContext(ProductsContext)
  const [departments] = useContext(DepartmentContext)

  const getDept = departmentId =>
    departments.find(dept => dept.departmentId === Number(departmentId))
  return (
    <Breadcrumb>
      <Breadcrumb.Section link>Home</Breadcrumb.Section>
      {state.department && (
        <>
          <Breadcrumb.Divider icon="right angle" />
          <Breadcrumb.Section link>
            {getDept(state.department).name}
          </Breadcrumb.Section>
        </>
      )}
      {state.category && (
        <>
          <Breadcrumb.Divider icon="right angle" />
          <Breadcrumb.Section link>{state.category}</Breadcrumb.Section>
        </>
      )}
      {state.page && (
        <>
          <Breadcrumb.Divider icon="right angle" />
          <Breadcrumb.Section active>{`Page ${state.page} of ${
            state.pages
          }`}</Breadcrumb.Section>
        </>
      )}
    </Breadcrumb>
  )
}

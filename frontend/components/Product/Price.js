import { Card } from "semantic-ui-react"

export const Price = ({ discountedPrice, price, quantity }) => {
  discountedPrice = Number(discountedPrice)
  price = Number(price)
  quantity = Number(quantity)
  return (
    <Card.Content extra className="prices">
      {discountedPrice ? (
        <>
          <span className="price">
            USD {(discountedPrice * quantity).toFixed(2)}
          </span>
          <span className="discounted">USD {price.toFixed(2)}</span>
        </>
      ) : (
        <span className="price">USD {(price * quantity).toFixed(2)}</span>
      )}
    </Card.Content>
  )
}

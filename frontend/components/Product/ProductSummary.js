import { useEffect, useState } from "react"
import { BASE_URL } from "../../config"
import { ProductCard } from "./ProductCard"

const useProduct = productId => {
  const [product, setProduct] = useState(null)
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(null)
  const url = `${BASE_URL}/products/${productId}`

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(product => {
        setProduct(product)
        setLoading(false)
      })
      .catch(error => {
        setLoading(false)
        setError(error)
        console.error(error)
      })
  }, [])

  return [product, loading, error]
}

const useProductAttributes = productId => {
  const [productAttributes, setProductAttributes] = useState([])
  const url = `${BASE_URL}/attributes/inProduct/${productId}`

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(productAttributes => {
        setProductAttributes(productAttributes)
      })
      .catch(error => {
        console.error(error)
      })
  }, [])

  return [productAttributes]
}

export const ProductSummary = ({ productId }) => {
  const [product, loading, error] = useProduct(productId)
  const [color, setColor] = useState("white")
  const [size, setSize] = useState("XL")
  const [quantity, setQuantity] = useState(1)
  const [productAttributes] = useProductAttributes(productId)
  const attributes = productAttributes.reduce((acc, curr) => {
    if (!acc[curr.attribute_name]) {
      acc[curr.attribute_name] = []
    }
    acc[curr.attribute_name].push(curr.attribute_value)
    return acc
  }, {})

  const onColorClick = color => {
    setColor(color)
  }
  const onSizeClick = size => {
    setSize(size)
  }

  const onQuantityChange = quantity => {
    if (quantity) {
      setQuantity(quantity)
    }
  }

  return (
    <div className="product__summary">
      {product && (
        <ProductCard
          product={product}
          ProductAttributes={attributes}
          isDetail={true}
          onColorClick={onColorClick}
          onSizeClick={onSizeClick}
          selectedColor={color}
          selectedSize={size}
          onQuantityChange={onQuantityChange}
          quantity={quantity}
        />
      )}
    </div>
  )
}

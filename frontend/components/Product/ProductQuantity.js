import { Input, Card } from "semantic-ui-react"

export const ProductQuantity = ({ quantity, onQuantityChange }) => (
  <Card.Content extra>
    <span>Quantity: </span>
    <br />
    <Input
      placeholder="Quantity"
      value={quantity}
      type="number"
      fluid
      min={1}
      onChange={evt => onQuantityChange(evt.target.value)}
    />
  </Card.Content>
)

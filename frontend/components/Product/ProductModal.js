import { Modal } from "semantic-ui-react"
import { ProductSummary } from "./ProductSummary"

export const ProductModal = props => (
  <Modal
    closeIcon="close"
    size="mini"
    trigger={props.trigger}
    className="info-modal"
  >
    <Modal.Header className="modal-header">{props.product.name}</Modal.Header>
    <Modal.Content className="product-modal__content">
      <ProductSummary productId={props.product.productId} />
    </Modal.Content>
  </Modal>
)

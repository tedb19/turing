import { useContext } from "react"
import Link from "next/link"
import { Menu } from "./Menu"
import "./styles.css"
import { ProductsContext } from "../../../context/ProductsContext"

export const Header = () => (
  <header>
    <Logo />
    <Menu />
  </header>
)

const Logo = () => {
  const [state, setState] = useContext(ProductsContext)
  const handleClick = evt => {
    evt.preventDefault()
    setState({ ...state, page: 1, department: "", category: "", products: [] })
  }
  return (
    <Link href="/">
      <img src="/static/tshirtshop.svg" alt="logo" />
    </Link>
  )
}

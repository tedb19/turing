import { Icon } from 'semantic-ui-react'
import { SignUpModal } from '../../SignUp/SignUpModal'

export const Menu = () => (
  <ul>
    <SignUpModal
      trigger={
        <li className="signup">
          Sign Up <Icon name="signup" />
        </li>
      }
      context="sign_up"
    />

    <SignUpModal
      trigger={
        <li className="signin">
          Login In <Icon name="sign in" />
        </li>
      }
      context="login"
    />
  </ul>
)

export const Content = ({ children }) => {
  return <section className="main-content">{children}</section>
}

import { SideBar } from "./SideBar"
import { Content } from "./Content"
import "./styles.css"

export const Main = ({ children }) => (
  <main>
    <SideBar />
    <Content>{children}</Content>
  </main>
)

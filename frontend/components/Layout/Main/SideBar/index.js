import { useState, useContext } from 'react'
import { Search, Header, Button } from 'semantic-ui-react'
import { ProductsContext } from '../../../../context/ProductsContext'
import {
  useDepartments,
  useDepartmentCategories
} from '../../../../hooks/useDepartments'
import { CartItem } from '../../../ShoppingCart/CartItem'

export const SideBar = () => {
  const [state, setState] = useContext(ProductsContext)
  const [departments] = useDepartments()
  const [departmentId, setDepartmentId] = useState(null)
  const [departmentCategories] = useDepartmentCategories(departmentId)
  const [activeDept, setActiveDept] = useState(null)

  const handleDeptClick = id => {
    setDepartmentId(id)
    setState(state => ({ ...state, department: id, page: 1 }))
    setActiveDept(id)
  }

  return (
    <aside>
      <Search
        icon="search"
        noResultsMessage="No results found"
        input="text"
        size="tiny"
        placeholder="Search..."
      />
      <Header as="h3">Choose A Department</Header>
      <section>
        {departments.map(department => (
          <Button
            onClick={() => handleDeptClick(department.departmentId)}
            key={department.name}
            className={activeDept === department.departmentId ? 'active' : ''}
          >
            {department.name}
          </Button>
        ))}
      </section>

      {departmentId && (
        <>
          <Header as="h3">Category</Header>
          <section>
            {departmentCategories.map(departmentCategory => (
              <Button key={departmentCategory.name}>
                {departmentCategory.name}
              </Button>
            ))}
          </section>
        </>
      )}

      <Header as="h3">Cart</Header>
      <CartItem
        cartItem={{
          quantity: 3,
          size: 'XL',
          color: 'Violet',
          name: "Char'd Ne"
        }}
      />
    </aside>
  )
}

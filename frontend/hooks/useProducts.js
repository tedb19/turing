import { useState, useEffect } from "react"
import { BASE_URL } from "../config"

export const useProducts = () => {
  const [state, setState] = useState({
    searchTerm: "",
    department: "",
    category: "",
    page: 1,
    pages: 0,
    products: [],
    loading: true
  })

  let url = `${BASE_URL}/products`
  url += state.department ? `/inDepartment/${state.department}` : ""
  url += state.page ? `?page=${state.page}` : ""

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(item => {
        setState({
          ...state,
          pages: item.pages,
          products: item.rows,
          loading: false
        })
      })
      .catch(error => {
        setState({ ...state, loading: false })
      })
  }, [state.department, state.page, url])

  return [state, setState]
}

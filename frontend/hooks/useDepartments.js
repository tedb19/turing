import { useState, useEffect } from "react"
import { BASE_URL } from "../config"

export const useDepartments = () => {
  const [departments, setDepartments] = useState([])
  const url = `${BASE_URL}/departments`

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(departments => {
        setDepartments(departments)
      })
      .catch(error => console.error(error))
  }, [])

  return [departments]
}

export const useDepartmentCategories = departmentId => {
  const [departmentCategories, setDepartmentCategories] = useState([])
  const url = `${BASE_URL}/categories/inDepartment/${departmentId}`

  useEffect(() => {
    if (departmentId) {
      fetch(url)
        .then(response => response.json())
        .then(departmentCategories => {
          setDepartmentCategories(departmentCategories)
        })
        .catch(error => console.error(error))
    }
  }, [departmentId])

  return [departmentCategories]
}

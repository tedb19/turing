import "semantic-ui-css/semantic.min.css"
import "./styles.css"
import { Page } from "../components/Page/Page"
import { Products } from "../components/Product/Products"
import { ProductPagination } from "../components/Product/ProductPagination"
import { BreadCrumbs } from "../components/Product/BreadCrumbs"

export default () => {
  return (
    <Page>
      <ProductPagination />
      <br />
      <BreadCrumbs />
      <Products />
    </Page>
  )
}

import { withRouter } from "next/router"
import { ProductSummary } from "../components/Product/ProductSummary"
import { Page } from "../components/Page/Page"

const Product = props => (
  <Page>
    <ProductSummary productId={props.router.query.id} />
  </Page>
)

export default withRouter(Product)

import { createContext } from "react"

export const DepartmentContext = createContext()

export const DepartmentProvider = DepartmentContext.Provider

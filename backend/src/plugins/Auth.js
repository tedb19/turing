const Models = require('../models')

const Auth = {
  name: 'Auth',
  version: '1.0.0',
  register: (server, options) => {
    const validate = async (request, token, h) => {
      let isValid = false
      const customer = await Models.Customer.findOne({ where: { token } })

      if (customer) {
        isValid = true
      }

      return { isValid, credentials: customer || {} }
    }

    server.auth.strategy('bearer', 'bearer-access-token', {
      validate
    })

    // server.auth.default('bearer')
  }
}

module.exports = Auth

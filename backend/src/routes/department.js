const { getDepartments, getDepartmentById } = require('./handlers/department')
const CreateHash = require('../utils/createHash')
const { generateErrorMessage } = require('../utils/errors')

const tags = ['departments', 'api', 'turing']

const departmentRoutes = [
  {
    method: 'GET',
    path: '/departments',
    handler: async (request, h) => {
      const departments = await getDepartments()
      const etag = CreateHash(departments)
      return h.response(departments).etag(etag)
    },
    options: {
      description: 'Get Departments',
      tags
    }
  },
  {
    method: 'GET',
    path: '/departments/{department_id}',
    handler: (request, h) => {
      if (isNaN(request.params.department_id)) {
        const errorMsg = generateErrorMessage({
          field: 'query_string',
          code: 'DEP_01'
        })
        return h.response(errorMsg).code(400)
      }

      return getDepartmentById(request.params.department_id)
    },
    options: {
      description: 'Get Department By ID',
      tags
    }
  }
]

module.exports = departmentRoutes

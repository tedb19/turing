const CreateHash = require('../utils/createHash')

const {
  getAttributes,
  getAttributeById,
  getAttributeValues,
  getProductAttributes
} = require('./handlers/attribute')

const tags = ['attributes', 'api', 'turing']

const attributeRoutes = [
  {
    method: 'GET',
    path: '/attributes',
    handler: async (request, h) => {
      const attributes = await getAttributes()
      const etag = CreateHash(attributes)
      return h.response(attributes).etag(etag)
    },
    options: {
      description: 'Get Attributes',
      tags
    }
  },
  {
    method: 'GET',
    path: '/attributes/{attribute_id}',
    handler: (request, h) => getAttributeById(request.params.attribute_id),
    options: {
      description: 'Get Attribute By ID',
      tags
    }
  },
  {
    method: 'GET',
    path: '/attributes/values/{attribute_id}',
    handler: (request, h) => getAttributeValues(request.params.attribute_id),
    options: {
      description: 'Get Values Attribute From Attribute',
      tags
    }
  },
  {
    method: 'GET',
    path: '/attributes/inProduct/{product_id}',
    handler: (request, h) => getProductAttributes(request.params.product_id),
    options: {
      description: 'Get all Attributes with Produt ID',
      tags
    }
  }
]

module.exports = attributeRoutes

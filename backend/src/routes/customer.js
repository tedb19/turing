const Jwt = require('jsonwebtoken')

const {
  getCustomerById,
  getCustomer,
  addCustomer,
  updateCustomer
} = require('./handlers/customer')
const { generateErrorMessage } = require('../utils/errors')

const tags = ['customers', 'api', 'turing']

const customerRoutes = [
  {
    method: 'POST',
    path: '/customers/login',
    handler: async (request, h) => {
      const { email, password } = request.payload
      const customer = await getCustomer({ email, password })

      if (!customer) {
        const errorMsg = generateErrorMessage({ code: 'AUT_02' })
        return h.response(errorMsg).code(401)
      }

      const { token } = customer
      return {
        customer: {
          schema: customer
        },
        accessToken: `Bearer ${token}`,
        expires_in: '24h'
      }
    },
    options: {
      description: 'Return a Object of Customer with auth credencials',
      tags
    }
  },
  {
    method: 'POST',
    path: '/customers',
    handler: async (request, h) => {
      const { email, name, password } = request.payload
      const customer = await addCustomer({
        email,
        name,
        password
      })
      const { customerId } = customer
      const token = await Jwt.sign({ customerId }, process.env.APP_SECRET)

      await updateCustomer({ token, customerId })

      return {
        customer: {
          schema: customer
        },
        accessToken: `Bearer ${token}`,
        expires_in: '24h'
      }
    },
    options: {
      description: 'Return a Object of Customer with auth credencials',
      tags
    }
  },
  {
    method: 'GET',
    path: '/customer',
    handler: async (request, h) => {
      const token = request.headers.authorization.replace(/Bearer /, '')
      const { customerId } = Jwt.verify(token, process.env.APP_SECRET)
      const customer = await getCustomerById(customerId)
      return customer
    },
    options: {
      description: 'Get a customer by token.',
      auth: 'bearer',
      tags
    }
  }
]

module.exports = customerRoutes

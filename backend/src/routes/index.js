const CategoryRoutes = require('./category')
const DepartmentRoutes = require('./department')
const ProductRoutes = require('./product')
const AttributeRoutes = require('./attribute')
const CustomerRoutes = require('./customer')
const ShoppingCartRoutes = require('./shoppingCart')

const routes = {
  name: 'T-Shirt Shop Backend API Endpoints',
  version: '1.0.0',
  register: (server, options) => {
    const allRoutes = [
      ...CategoryRoutes,
      ...DepartmentRoutes,
      ...ProductRoutes,
      ...AttributeRoutes,
      ...CustomerRoutes,
      ...ShoppingCartRoutes
    ]
    server.route(allRoutes)
  }
}

module.exports = routes

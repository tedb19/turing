const CreateHash = require('../utils/createHash')

const {
  getCategories,
  getCategoryById,
  getProductCategories,
  getCategoriesInDepartment
} = require('./handlers/category')

const tags = ['categories', 'api', 'turing']

const categoryRoutes = [
  {
    method: 'GET',
    path: '/categories',
    handler: async (request, h) => {
      const categories = await getCategories()
      const etag = CreateHash(categories)
      return h.response(categories).etag(etag)
    },
    options: {
      description: 'Get Categories',
      tags
    }
  },
  {
    method: 'GET',
    path: '/categories/{category_id}',
    handler: (request, h) => getCategoryById(request.params.category_id),
    options: {
      description: 'Get Category By ID',
      tags
    }
  },
  {
    method: 'GET',
    path: '/categories/inProduct/{product_id}',
    handler: (request, h) => getProductCategories(request.params.product_id),
    options: {
      description: 'Get Categories Of A Product',
      tags
    }
  },
  {
    method: 'GET',
    path: '/categories/inDepartment/{department_id}',
    handler: (request, h) =>
      getCategoriesInDepartment(request.params.department_id),
    options: {
      description: 'Get Categories Of A Department',
      tags
    }
  }
]

module.exports = categoryRoutes

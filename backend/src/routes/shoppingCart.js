const Uuidv4 = require('uuid/v4')
const { addProductToCart, getCartProducts } = require('./handlers/shoppinCart')

const tags = ['shopping cart', 'api', 'turing']

const ShoppingCartRoutes = [
  {
    method: 'GET',
    path: '/shoppingcart/generateUniqueId',
    handler: (request, h) => {
      const cart_id = Uuidv4().slice(0, 32)
      return { cart_id }
    },
    options: {
      description: 'Generete the unique CART ID',
      tags
    }
  },
  {
    method: 'GET',
    path: '/shoppingcart/{cart_id}',
    handler: (request, h) => {
      const { cart_id } = request.params
      return getCartProducts(cart_id)
    },
    options: {
      description: 'Get list of products in shopping cart',
      tags
    }
  },
  {
    method: 'POST',
    path: '/shoppingcart/add',
    handler: async (request, h) => {
      const { cart_id } = request.payload
      await addProductToCart(request.payload)
      return getCartProducts(cart_id)
    },
    options: {
      description: 'Add a product into the cart',
      tags
    }
  }
]

module.exports = ShoppingCartRoutes

const Boom = require('boom')

const {
  getProducts,
  getProductsBySearch,
  getProductById,
  getProductsInCategory,
  getProductsInDepartment
} = require('./handlers/product')

const { generateErrorMessage } = require('../utils/errors')

const tags = ['products', 'api', 'turing']

const productsRoutes = [
  {
    method: 'GET',
    path: '/products',
    handler: (request, h) => {
      let { page, limit, description_length } = request.query
      if (!page) page = 1
      if (!limit) limit = 20
      if (!description_length) description_length = 200
      return getProducts({ page, limit, description_length })
    },
    options: {
      description: 'Get all products',
      tags
    }
  },
  {
    method: 'GET',
    path: '/products/search',
    handler: (request, h) => {
      let { page, limit, description_length, query_string } = request.query
      if (!page) page = 1
      if (!limit) limit = 20
      if (!description_length) description_length = 200
      query_string = query_string ? query_string.trim().toLowerCase() : ''
      if (!query_string) {
        const errorMsg = generateErrorMessage({
          field: 'query_string',
          code: 'USR_02'
        })
        return h.response(errorMsg).code(400)
      }

      return getProductsBySearch({
        query_string,
        page,
        limit,
        description_length
      })
    },
    options: {
      description: 'Search Products',
      tags
    }
  },
  {
    method: 'GET',
    path: '/products/{product_id}',
    handler: async (request, h) => {
      const { product_id } = request.params
      if (!product_id) {
        const errorMsg = generateErrorMessage({
          field: 'product_id',
          code: 'USR_02'
        })
        return h.response(errorMsg).code(400)
      }

      const product = await getProductById(product_id)
      if (product) {
        return product
      }

      return Boom.notFound()
    },
    options: {
      description: 'Return a Product Object',
      tags
    }
  },
  {
    method: 'GET',
    path: '/products/inCategory/{category_id}',
    handler: (request, h) => {
      const { category_id } = request.params
      let { page, description_length, limit } = request.query
      if (!page) page = 1
      if (!limit) limit = 20
      if (!description_length) description_length = 200
      return getProductsInCategory({
        categoryId: category_id,
        page,
        description_length,
        limit
      })
    },
    options: {
      description: 'Get a list of Products in Category',
      tags
    }
  },
  {
    method: 'GET',
    path: '/products/inDepartment/{department_id}',
    handler: (request, h) => {
      const { department_id } = request.params
      let { page, description_length, limit } = request.query
      if (!page) page = 1
      if (!limit) limit = 20
      if (!description_length) description_length = 200
      return getProductsInDepartment({
        departmentId: department_id,
        page,
        description_length,
        limit
      })
    },
    options: {
      description: 'Get a list of Products on Department',
      tags
    }
  }
]

module.exports = productsRoutes

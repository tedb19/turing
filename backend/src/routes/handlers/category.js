const Models = require('../../models')
const { getProductById } = require('./product')
const { getDepartmentById } = require('./department')

const getCategories = async () => await Models.Category.findAll()

const getCategoryById = async id => await Models.Category.findByPk(id)

const getProductCategories = async product_id => {
  const product = await getProductById(product_id)
  return product.getCategories()
}

const getCategoriesInDepartment = async department_id => {
  const department = await getDepartmentById(department_id)
  return department.getCategories()
}

module.exports = {
  getCategories,
  getCategoryById,
  getProductCategories,
  getCategoriesInDepartment
}

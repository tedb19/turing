const Models = require('../../models')
const { getProductById } = require('./product')

const getAttributes = async () => await Models.Attribute.findAll()

const getAttributeById = async id => await Models.Attribute.findByPk(id)

const getAttributeValues = async attributeId => {
  const attribute = await getAttributeById(attributeId)
  return attribute.getAttributeValues()
}

const getProductAttributes = async productId => {
  const product = await getProductById(productId)
  const attributeValues = await product.getAttributeValues()
  return Promise.all(
    attributeValues.map(async attributeValue => {
      const attribute = await getAttributeById(attributeValue.attribute_id)
      return {
        attribute_name: attribute.name,
        attribute_value_id: attributeValue.attributeValueId,
        attribute_value: attributeValue.value
      }
    })
  )
}

module.exports = {
  getAttributes,
  getAttributeById,
  getAttributeValues,
  getProductAttributes
}

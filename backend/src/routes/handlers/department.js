const Models = require('../../models')

const getDepartments = async () => await Models.Department.findAll()

const getDepartmentById = async id => await Models.Department.findByPk(id)

module.exports = {
  getDepartments,
  getDepartmentById
}

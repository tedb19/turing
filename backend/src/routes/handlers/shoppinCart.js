const Models = require('../../models')

const addProductToCart = cart =>
  Models.ShoppingCart.create({ ...cart, cartId: cart.cart_id })

const getCartProducts = cart_id =>
  Models.ShoppingCart.findAll({ where: { cart_id } })

module.exports = {
  addProductToCart,
  getCartProducts
}

const Models = require('../../models')

const getCustomerById = id => Models.Customer.findByPk(id)
const getCustomer = where =>
  Models.Customer.findOne({
    where
  })

const addCustomer = ({ email, name, password }) =>
  Models.Customer.create({
    email,
    name,
    password
  })

const updateCustomer = ({ token, customerId }) =>
  Models.Customer.update(
    { token },
    {
      where: { customerId }
    }
  )
module.exports = {
  getCustomerById,
  getCustomer,
  addCustomer,
  updateCustomer
}

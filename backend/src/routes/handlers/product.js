const Models = require('../../models')

const { getDepartmentById } = require('./department')

const Op = Models.Sequelize.Op

const getProducts = async ({
  page,
  limit,
  description_length,
  moreOptions = {}
}) => {
  page = Number(page) - 1
  limit = Number(limit)
  const offset = page * limit

  try {
    const options = {
      limit,
      offset,
      attributes: {
        exclude: ['description'],
        include: [
          [
            Models.sequelize.fn(
              'LEFT',
              Models.sequelize.col('description'),
              description_length
            ),
            'description'
          ]
        ]
      },
      order: [['product_id', 'ASC']],
      ...moreOptions
    }

    const data = await Models.Product.findAndCountAll(options)
    const { count, rows } = data
    const pages = Math.ceil(count / limit)
    return { count, pages, rows }
  } catch (err) {
    console.error(err)
  }
}

const getProductsBySearch = async ({
  query_string,
  page,
  limit,
  description_length
}) => {
  const like = {
    [Op.like]: `%${query_string}%`
  }
  const moreOptions = {
    where: {
      [Op.or]: [{ description: like }, { name: like }]
    }
  }

  try {
    const data = await getProducts({
      page,
      limit,
      description_length,
      moreOptions
    })
    const { count, rows } = data
    const pages = Math.ceil(count / limit)
    return { count, pages, rows }
  } catch (err) {
    console.error(err)
  }
}

const getProductById = async id => await Models.Product.findByPk(id)

const getProductsInCategory = async ({
  categoryId,
  page,
  description_length,
  limit
}) => {
  const moreOptions = {
    include: [
      {
        model: Models.Category,
        where: { category_id: categoryId }
      }
    ]
  }
  const data = await getProducts({
    page,
    limit,
    description_length,
    moreOptions
  })

  const { count, rows } = data
  const pages = Math.ceil(count / limit)
  return { count, pages, rows }
}

const getProductsInDepartment = async ({
  departmentId,
  page,
  description_length,
  limit
}) => {
  const department = await getDepartmentById(departmentId)
  const categories = await department.getCategories()
  const categoryIds = categories.map(category => category.categoryId)
  const moreOptions = {
    include: [
      {
        model: Models.Category,
        where: {
          category_id: {
            [Op.in]: categoryIds
          }
        }
      }
    ]
  }
  const data = await getProducts({
    page,
    limit,
    description_length,
    moreOptions
  })

  const { count, rows } = data
  const pages = Math.ceil(count / limit)
  return { count, pages, rows }
}

module.exports = {
  getProducts,
  getProductById,
  getProductsBySearch,
  getProductsInCategory,
  getProductsInDepartment
}

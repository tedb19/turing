const Crypto = require('crypto')

module.exports = data => {
  return Crypto.createHash('sha1')
    .update(JSON.stringify(data))
    .digest('hex')
}

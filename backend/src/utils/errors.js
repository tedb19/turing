const generateErrorMessage = ({ code, field = 'N/A', status = '400' }) => {
  const errors = {
    USR_01: 'Email or Password is invalid.',
    USR_02: 'The field(s) are/is required.',
    USR_03: 'The email is invalid.',
    USR_04: 'The email already exists.',
    USR_05: 'The email doesn\'t exist.',
    USR_06: 'this is an invalid phone number.',
    USR_07: `this is too long ${field}.`,
    USR_08: 'this is an invalid Credit Card.',
    USR_09: 'The Shipping Region ID is not number',
    PAG_01: 'The order is not matched \'field,(DESC|ASC)\'.',
    PAG_02: 'The field of order is not allow sorting.',
    AUT_01: 'Authorization code is empty.',
    AUT_02: 'Access Unauthorized.',
    CAT_01: 'Don\'t exist category with this ID.',
    DEP_01: 'The ID is not a number.',
    DEP_02: 'Don\'exist department with this ID.'
  }

  return {
    code,
    message: errors[code],
    field,
    status
  }
}

module.exports = {
  generateErrorMessage
}

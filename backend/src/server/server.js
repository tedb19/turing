const Glue = require('glue')
const Manifest = require('../config/manifest')

const options = {
  relativeTo: __dirname
}

exports.createServer = async () => {
  return await Glue.compose(
    Manifest,
    options
  )
}

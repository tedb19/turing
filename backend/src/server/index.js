const { createServer } = require('./server')

require('dotenv').config({ path: '.env' })

const init = async () => {
  try {
    const server = await createServer()
    server.start()
    console.log(`✅  Server started @ ${server.info.uri}`)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

process.on('unhandledRejection', error => {
  console.error(error)
  process.exit(1)
})

init()

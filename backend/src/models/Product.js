module.exports = function(sequelize, DataTypes) {
  const Product = sequelize.define(
    'Product',
    {
      productId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'product_id'
      },
      name: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'name'
      },
      description: {
        type: DataTypes.STRING(1000),
        allowNull: false,
        field: 'description'
      },
      price: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        field: 'price'
      },
      discountedPrice: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        defaultValue: '0.00',
        field: 'discounted_price'
      },
      image: {
        type: DataTypes.STRING(150),
        allowNull: true,
        field: 'image'
      },
      image2: {
        type: DataTypes.STRING(150),
        allowNull: true,
        field: 'image_2'
      },
      thumbnail: {
        type: DataTypes.STRING(150),
        allowNull: true,
        field: 'thumbnail'
      },
      display: {
        type: DataTypes.INTEGER(6),
        allowNull: false,
        defaultValue: '0',
        field: 'display'
      }
    },
    {
      tableName: 'product',
      timestamps: false
    }
  )

  Product.associate = function(models) {
    Product.belongsToMany(models.AttributeValue, {
      foreignKey: 'product_id',
      through: 'product_attribute',
      timestamps: false
    })
    Product.belongsToMany(models.Category, {
      foreignKey: 'product_id',
      through: 'product_category',
      timestamps: false
    })
    Product.hasMany(models.Review, { foreignKey: 'product_id' })
    Product.hasMany(models.ShoppingCart, { foreignKey: 'product_id' })
  }

  return Product
}

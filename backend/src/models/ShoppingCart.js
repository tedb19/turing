module.exports = function(sequelize, DataTypes) {
  const ShoppingCart = sequelize.define(
    'ShoppingCart',
    {
      itemId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'item_id'
      },
      cartId: {
        type: DataTypes.CHAR(32),
        allowNull: false,
        field: 'cart_id'
      },
      attributes: {
        type: DataTypes.STRING(1000),
        allowNull: false,
        field: 'attributes'
      },
      quantity: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        field: 'quantity'
      },
      buyNow: {
        type: DataTypes.INTEGER(1),
        allowNull: false,
        defaultValue: '1',
        field: 'buy_now'
      },
      addedOn: {
        type: DataTypes.DATE,
        allowNull: false,
        field: 'added_on',
        defaultValue: DataTypes.NOW
      }
    },
    {
      tableName: 'shopping_cart',
      timestamps: false
    }
  )

  return ShoppingCart
}

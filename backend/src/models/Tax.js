module.exports = function(sequelize, DataTypes) {
  const Tax = sequelize.define(
    'Tax',
    {
      taxId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'tax_id'
      },
      taxType: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'tax_type'
      },
      taxPercentage: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        field: 'tax_percentage'
      }
    },
    {
      tableName: 'tax',
      timestamps: false
    }
  )

  Tax.associate = function(models) {
    Tax.hasMany(models.Order, { foreignKey: 'tax_id' })
  }

  return Tax
}

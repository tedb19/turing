module.exports = function(sequelize, DataTypes) {
  const Order = sequelize.define(
    'Order',
    {
      orderId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'order_id'
      },
      totalAmount: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        defaultValue: '0.00',
        field: 'total_amount'
      },
      createdOn: {
        type: DataTypes.DATE,
        allowNull: false,
        field: 'created_on',
        defaultValue: DataTypes.NOW
      },
      shippedOn: {
        type: DataTypes.DATE,
        allowNull: true,
        field: 'shipped_on'
      },
      status: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        defaultValue: '0',
        field: 'status'
      },
      comments: {
        type: DataTypes.STRING(255),
        allowNull: true,
        field: 'comments'
      },
      authCode: {
        type: DataTypes.STRING(50),
        allowNull: true,
        field: 'auth_code'
      },
      reference: {
        type: DataTypes.STRING(50),
        allowNull: true,
        field: 'reference'
      }
    },
    {
      tableName: 'orders',
      timestamps: false
    }
  )

  Order.associate = function(models) {
    Order.hasMany(models.Audit, { foreignKey: 'order_id' })
  }

  return Order
}

module.exports = function(sequelize, DataTypes) {
  const AttributeValue = sequelize.define(
    'AttributeValue',
    {
      attributeValueId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'attribute_value_id'
      },
      value: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'value'
      }
    },
    {
      tableName: 'attribute_value',
      timestamps: false
    }
  )
  AttributeValue.associate = function(models) {
    AttributeValue.belongsToMany(models.Product, {
      foreignKey: 'attribute_value_id',
      through: 'product_attribute',
      timestamps: false
    })
  }

  return AttributeValue
}

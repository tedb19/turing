module.exports = function(sequelize, DataTypes) {
  const Shipping = sequelize.define(
    'Shipping',
    {
      shippingId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'shipping_id'
      },
      shippingType: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'shipping_type'
      },
      shippingCost: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        field: 'shipping_cost'
      }
    },
    {
      tableName: 'shipping',
      timestamps: false
    }
  )
  Shipping.associate = function(models) {
    Shipping.hasMany(models.Order, { foreignKey: 'shipping_id' })
  }

  return Shipping
}

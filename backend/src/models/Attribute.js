module.exports = function(sequelize, DataTypes) {
  const Attribute = sequelize.define(
    'Attribute',
    {
      attributeId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'attribute_id'
      },
      name: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'name'
      }
    },
    {
      tableName: 'attribute',
      timestamps: false
    }
  )

  Attribute.associate = function(models) {
    Attribute.hasMany(models.AttributeValue, { foreignKey: 'attribute_id' })
  }

  return Attribute
}

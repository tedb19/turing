module.exports = function(sequelize, DataTypes) {
  const OrderDetail = sequelize.define(
    'OrderDetail',
    {
      itemId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'item_id'
      },
      productId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        field: 'product_id'
      },
      attributes: {
        type: DataTypes.STRING(1000),
        allowNull: false,
        field: 'attributes'
      },
      productName: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'product_name'
      },
      quantity: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        field: 'quantity'
      },
      unitCost: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        field: 'unit_cost'
      }
    },
    {
      tableName: 'order_detail',
      timestamps: false
    }
  )
  OrderDetail.associate = function(models) {
    OrderDetail.belongsTo(models.Order, {
      foreignKey: 'order_id'
    })
  }

  return OrderDetail
}

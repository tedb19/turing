module.exports = function(sequelize, DataTypes) {
  const ShippingRegion = sequelize.define(
    'ShippingRegion',
    {
      shippingRegionId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'shipping_region_id'
      },
      shippingRegion: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'shipping_region'
      }
    },
    {
      tableName: 'shipping_region',
      timestamps: false
    }
  )

  ShippingRegion.associate = function(models) {
    ShippingRegion.hasMany(models.Customer, {
      foreignKey: 'shipping_region_id'
    })
    ShippingRegion.hasMany(models.Shipping, {
      foreignKey: 'shipping_region_id'
    })
  }

  return ShippingRegion
}

module.exports = function(sequelize, DataTypes) {
  const Department = sequelize.define(
    'Department',
    {
      departmentId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'department_id'
      },
      name: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'name'
      },
      description: {
        type: DataTypes.STRING(1000),
        allowNull: true,
        field: 'description'
      }
    },
    {
      tableName: 'department',
      timestamps: false
    }
  )

  Department.associate = function(models) {
    Department.hasMany(models.Category, { foreignKey: 'department_id' })
  }

  return Department
}

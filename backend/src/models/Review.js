module.exports = function(sequelize, DataTypes) {
  const Review = sequelize.define(
    'Review',
    {
      reviewId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'review_id'
      },
      review: {
        type: DataTypes.TEXT,
        allowNull: false,
        field: 'review'
      },
      rating: {
        type: DataTypes.INTEGER(6),
        allowNull: false,
        field: 'rating'
      },
      createdOn: {
        type: DataTypes.DATE,
        allowNull: false,
        field: 'created_on',
        defaultValue: DataTypes.NOW
      }
    },
    {
      tableName: 'review',
      timestamps: false
    }
  )
  return Review
}

module.exports = function(sequelize, DataTypes) {
  const Audit = sequelize.define(
    'Audit',
    {
      auditId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'audit_id'
      },
      createdOn: {
        type: DataTypes.DATE,
        allowNull: false,
        field: 'created_on',
        defaultValue: DataTypes.NOW
      },
      message: {
        type: DataTypes.TEXT,
        allowNull: false,
        field: 'message'
      },
      code: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        field: 'code'
      }
    },
    {
      tableName: 'audit',
      timestamps: false
    }
  )
  return Audit
}

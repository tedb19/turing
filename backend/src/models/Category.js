module.exports = function(sequelize, DataTypes) {
  const Category = sequelize.define(
    'Category',
    {
      categoryId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        field: 'category_id'
      },
      name: {
        type: DataTypes.STRING(100),
        allowNull: false,
        field: 'name'
      },
      description: {
        type: DataTypes.STRING(1000),
        allowNull: true,
        field: 'description'
      }
    },
    {
      tableName: 'category',
      timestamps: false
    }
  )

  Category.associate = function(models) {
    Category.belongsToMany(models.Product, {
      foreignKey: 'category_id',
      through: 'product_category',
      timestamps: false
    })
  }

  return Category
}

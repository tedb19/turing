### TShirt Shop:

This is the `Turing Fullstack Interview Project`.
You can check it out [here](https://url.com/)

### Technology Stack:

#### Backend (nodejs):

**_Hapijs_** - To create secure REST APIs
**_Glue_** - A `hapijs` plugin for composing a server using a manifest file.
**_Sequelize_** - The ORM used to interact with the mysql database
**_Bunyan_** - The logging library used to manage logging

#### Frontend (reactjs):

**_nextjs_** - A `reactjs` framework that provides capabilities for supporting `SSR (Server Side rendering)`, `code-splitting`, `zero-config routing` etc
**_React Semantic UI_** - A component based `UI framework` that is a joy to use.

### Deployment:

#### Static Files:

- We are using the `cloudinary` api for managing our images. Cloudinary's built-in fast CDN delivery helps to get all resources to the client quickly. It also provides a simple API for uploading files to a safe cloud storage and simple access to managed files through dynamic URLs

#### TODO:
